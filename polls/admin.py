# Register your models here.
from django.contrib import admin
from .models import Question, Choice, Author, Post

admin.site.register(Question)
admin.site.register(Choice)
admin.site.register(Post)
admin.site.register(Author)