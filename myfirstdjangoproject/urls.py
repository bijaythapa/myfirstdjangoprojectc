"""myfirstdjangoproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.contrib import admin
# from django.conf.urls import url
from django.urls import path
from account import views
# from basic_app import views
# from polls import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', include('login.urls')),
    path('', views.dashboard),

    # url('polls/', views.detail),
    path('polls/', include('polls.urls')),

    path('account/', include('account.urls')),

    path('userlogin/', include('userlogin.urls')),

    path('basic_app/', include('basic_app.urls')),
    # path('logout/', views.user_logout)
    # path('special/', views.special)

    # only one call on app_urls
    # url('', views.detail),
    # url('', views.results),
    # url('', views.vote),

]
