from django.urls import path
from basic_app import views

app_name = 'basic_app'
urlpatterns = [
    path('', views.index, name='basic_index'),
    path('register/', views.register, name='basic_register'),
    path('login/', views.login, name='login'),
    path('logout/', views.user_logout, name='user_logout'),
    path('special/', views.special, name='special'),
]
