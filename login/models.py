from django.db import models


class Person(models.Model):
    user_name = models.CharField(max_length=10)
    password = models.CharField(max_length=8)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

    def __str__(self):
        return 'user_name: {} password: {} first_name: {} last_name: {}'.format(self.user_name, self.password,
                                                                                self.first_name, self.last_name)


class Student(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    student_name = models.CharField(max_length=10)
    student_roll = models.IntegerField(default=0)

    def __str__(self):
        return 'student_name: {} student_roll: {}'.format(self.student_name, self.student_roll)


class Snippet(models.Model):
    name = models.CharField(max_length=20)
    subject = models.CharField(max_length=10)
    email = models.EmailField()
    # cc_myself = models.BooleanField()

    def __str__(self):
        return 'name: {} subject: {} email: {}'.format(self.name, self.subject, self.email)
