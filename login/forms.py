from random import choices

from django import forms

from login.models import Snippet


class FormName(forms.Form):
    name = forms.CharField(label='name', max_length=20, widget=[])
    subject = forms.CharField(max_length=10)
    # category = forms.ChoiceField(choices=[('question', 'Question'), ('other', 'Other')])
    message = forms.CharField(widget=forms.Textarea)
    email = forms.EmailField()
    cc_myself = forms.BooleanField(required=False)


class SnippetForm(forms.ModelForm):
    class Meta:
        model = Snippet
        fields = ('name', 'subject', 'email')
