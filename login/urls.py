from django.urls import path

from . import views

urlpatterns = [
    path('index', views.index, name='index'),
    path('check', views.check, name='check'),
    path('formpage/', views.form_one, name='form_name'),
    path('snippet/', views.snippet_detail, name='snippet_detail')
]

