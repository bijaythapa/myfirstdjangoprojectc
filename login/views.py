from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
# from django.core.mail import send_mail
# from . import forms
#
# from django.views.generic import ListView

# class IndexView(TemplateView):
# template_name = 'index.html'
from .forms import FormName, SnippetForm


def index(request):
    return render(request, "index.html")


def check(request):
    return render(request, "check.html")


def form_one(request):
    if request.method == 'POST':
        form = FormName(request.POST)
        print("hello from up of is_valid in snippet")

        if form.is_valid():
            name = form.cleaned_data['name']
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            sender = form.cleaned_data['sender']
            cc_myself = form.cleaned_data['cc_myself']

            print(name, subject, message, sender, cc_myself)

            # recipient = ['bijay@gmail.com']
            # if cc_myself:
            #     recipient.append(sender)
            #
            # send_mail(name, subject, message, sender, recipient)

            return HttpResponseRedirect("Thanks, Successfully Submitted !!", 'form_one')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = FormName()
        print("didnt go inside 'post'")
    return render(request, 'form_one.html', {'form': form})


def snippet_detail(request):
    if request.method == "POST":
        form = SnippetForm(request.POST)
        print("hello from up of is_valid in snippet")
        if form.is_valid():
            form.save()
            print("values saved in database")
            return redirect('snippet_detail')
    else:
        form = SnippetForm()
        print("didn't go inside post method")
    return render(request, 'model_field.html', {'form': form})


# class HomeList(ListView):
#     queryset = HomeList
#     template_name = ''
#     context_object_name = 'homes'
#
#     def get_context_data(self, *, object_list=None, **kwargs):
#         return dictionary
#
#     def get(self, request, *args, **kwargs):
#         orm = SnippetForm()
#         print("didn't go inside post method")
#
#         return render(request, 'model_field.html', {'form': form})
