# Register your models here.
from django.contrib import admin
from .models import Person, Snippet, Student

admin.site.register(Person)
admin.site.register(Student)
admin.site.register(Snippet)
