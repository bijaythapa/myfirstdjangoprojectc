# from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic.base import View
from userlogin.models import UserRegister
from userlogin.forms import UserRegisterForm, UserLoginForm
# from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist


# def user_register(request):
#   if request.method == "post":
#     form = UserRegisterForm(request.POST)
#     if form.is_valid():
#         form.save()
#         return redirect('user_register')
#   else:
#     form = UserRegisterForm()
#     return render(request, 'userlogin/user_register.html', {'form': form})
#
#
# def user_login(request):
#     form = UserLoginForm()
#     return render(request, 'userlogin/user_login.html', {'form': form})


class UserRegistration(View):
    model = UserRegister
    initial = {'key': 'value'}
    form_class = UserRegisterForm
    template_name = 'userlogin/user_register.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            return redirect('user_login')
            # return HttpResponseRedirect('/success')
        return render(request, self.template_name, {'form': form})


class UserLogging(View):
    model = UserRegister
    form_class = UserLoginForm
    template_name = 'userlogin/user_login.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            # import pdb; pdb.set_trace()
            data = form.cleaned_data
            # user = form.data.get("user_name")
            # passw = form.data.get("password1")
            # user = auth.authenticate(username=usern, password=passw)
            user1 = data['user_name']
            pass1 = data['password1']

            try:
                user2 = UserRegister.objects.get(user_name=user1)
                pass2 = user2.password1
                if pass1 == pass2:
                    messages.success(request, "You're Successfully logged In !!")
                    return redirect('user_register')
                else:
                    messages.warning(request, "Password didn't matched!!")
                    return redirect('user_login')
            except ObjectDoesNotExist as abc:
                form = self.form_class(request.POST)
                error = "User name does not Exists"
                # messages.warning("Username does not exists !!")
                return render(request, self.template_name, {'error': error, 'form': form})
