from django.urls import path
from . import views

urlpatterns = [
    path('register/', views.UserRegistration.as_view(), name="user_register"),
    path('login/', views.UserLogging.as_view(), name="user_login"),
]
