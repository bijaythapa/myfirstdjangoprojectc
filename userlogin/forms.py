from django import forms

from userlogin.models import UserRegister


class UserRegisterForm(forms.ModelForm):
    class Meta:
        model = UserRegister
        widgets = {
            'password1': forms.PasswordInput(),
            'password2': forms.PasswordInput(),
        }
        fields = '__all__'


class UserLoginForm(forms.ModelForm):
    class Meta:
        model = UserRegister
        widgets = {
            'password1': forms.PasswordInput()
        }
        fields = ('user_name', 'password1')
