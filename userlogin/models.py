from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.

# class UserType(AbstractUser):
#     is_admin = models.BooleanField(default=False)
#     is_user = models.BooleanField(default=False)


USER_TYPES = (
    ('admin', 'admin_user'),
    ('user', 'normal_user'),
)


class UserRegister(models.Model):
    # user = models.OneToOneField(UserType, on_delete=models.CASCADE, primary_key=True)
    first_name = models.CharField(max_length=10)
    last_name = models.CharField(max_length=10)
    user_name = models.CharField(max_length=10)
    email = models.EmailField(max_length=50)
    password1 = models.CharField(max_length=10)
    password2 = models.CharField(max_length=10)
    user_type = models.CharField(max_length=11, choices=USER_TYPES, default='user')

    def __str__(self):
        return '{} {} {} {} {} {} {}'.format(self.first_name, self.last_name, self.password1, self.user_name, self.email
                                             , self.password1, self.user_type)

# class UserLogin(models.Model):
#     user_name = models.CharField(max_length=10)
#     password = models.CharField(max_length=10)
#
#     def __str__(self):
#         return '{} {}'.format(self.user_name, self.password)
