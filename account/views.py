from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User, auth


# Create your views here.

def register(request):
    return render(request, 'account/account.html')


def user_register(request):
    print("inside register view")
    if request.method == "POST":
        print("inside Post")

        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        username = request.POST['user_name']
        email = request.POST['email']
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        if password1 == password2:
            if User.objects.filter(username=username).exists():
                print("Username is taken, try another !!")
                messages.info(request, "Username is already taken. Try another one !!")
                return redirect('register')
            elif User.objects.filter(email=email).exists():
                print("Invalid email. Please insert valid email !!")
                messages.warning(request, "Invalid Email !!")
                return redirect('register')
            else:
                user = User.objects.create_user(
                    username=username, first_name=first_name, last_name=last_name, password=password1, email=email
                )
                user.save()
                print("User Created")
                messages.success(request, "Successfully Registered !!")
                return redirect("login")

        else:
            print("Passwords did not match !!")
            messages.info(request, "Password did not match !!")
            return redirect('register')
    else:
        return render(request, 'account/account.html')


def login(request):
    return render(request, 'account/login.html')


def user_login(request):
    if request.method == "POST":
        print("inside login-post")
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)
        print(user)
        if user is not None:
            auth.login(request, user)
            messages.success(request, "Successfully Logged-In !!")
            return redirect("/")
        else:
            messages.warning(request, "Invalid Credentials !!")
            return render(request, "account/login.html", {'username': username, 'password': password})
    else:
        return render(request, 'account/login.html')


def logout(request):
    auth.logout(request)
    messages.success(request, "Successfully Logged-Out")
    return redirect('/')


def dashboard(request):
    return render(request, 'account/dashboard.html')
